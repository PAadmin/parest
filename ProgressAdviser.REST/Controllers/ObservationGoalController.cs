﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ProgressAdviser.REST.Models;
using System.Threading.Tasks;

namespace ProgressAdviser.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using CH2MHILL.PMP2.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ObservationsGoal>("ObservationsGoal");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ObservationsGoalController : ODataController
    {
        private progressAdviserEntities db = new progressAdviserEntities();

        // GET odata/ObservationsGoal
        [Queryable]
        public IQueryable<tblObservationsGoal> GetObservationsGoal()
        {
            return db.tblObservationsGoals;
        }

        // GET odata/ObservationsGoal(5)
        [Queryable]
        public SingleResult<tblObservationsGoal> GetObservationsGoal([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblObservationsGoals.Where(ObservationsGoal => ObservationsGoal.goalID == key));
        }

        // PUT odata/ObservationsGoal(5)
        //public IHttpActionResult Put([FromODataUri] int key, ObservationsGoal ObservationsGoal)
        //public HttpResponseMessage Put([FromODataUri] int key, ObservationsGoal ObservationsGoal)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, tblObservationsGoal ObservationsGoal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != ObservationsGoal.Id)
            //{
            //    return BadRequest();
            //}

            db.Entry(ObservationsGoal).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!ObservationsGoalExists(key))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return Updated(ObservationsGoal);
            //return Request.CreateResponse<ObservationsGoal[]>(HttpStatusCode.OK, new[] { ObservationsGoal });
        }

        // POST odata/ObservationsGoal
        //public IHttpActionResult Post(ObservationsGoal ObservationsGoal)
        public HttpResponseMessage Post(tblObservationsGoal ObservationsGoal)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblObservationsGoals.Add(ObservationsGoal);
            db.SaveChanges();

            //return Created(ObservationsGoal);
            return Request.CreateResponse <tblObservationsGoal[]>(HttpStatusCode.Created, new[] { ObservationsGoal });
        }

        //// PATCH odata/ObservationsGoal(5)
        //[AcceptVerbs("PATCH", "MERGE")]
        //public IHttpActionResult Patch([FromODataUri] int key, Delta<ObservationsGoal> patch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    ObservationsGoal ObservationsGoal = db.ObservationsGoals.Find(key);
        //    if (ObservationsGoal == null)
        //    {
        //        return NotFound();
        //    }

        //    patch.Patch(ObservationsGoal);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ObservationsGoalExists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Updated(ObservationsGoal);
        //}

        // DELETE odata/ObservationsGoal(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblObservationsGoal ObservationsGoal = db.tblObservationsGoals.Find(key);
            if (ObservationsGoal == null)
            {
                return NotFound();
            }

            foreach (tblObservationsGoal rec in db.tblObservationsGoals.Where(e => e.goalID == key))
            {
                db.tblObservationsGoals.Remove(rec);
            }

            db.tblObservationsGoals.Remove(ObservationsGoal);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ObservationsGoal ObservationsGoal = db.ObservationsGoals.Find(key);
        //    if (ObservationsGoal == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ObservationsGoals.Remove(ObservationsGoal);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObservationsGoalExists(int key)
        {
            return db.tblObservationsGoals.Count(e => e.goalID == key) > 0;
        }
    }
}
