﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ProgressAdviser.REST.Models;
using System.Threading.Tasks;

namespace ProgressAdviser.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using CH2MHILL.PMP2.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Observation>("Observation");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ObservationController : ODataController
    {
        private progressAdviserEntities db = new progressAdviserEntities();

        // GET odata/Observation
        [Queryable]
        public IQueryable<tblObservation> GetObservation()
        {
            return db.tblObservations;
        }

        // GET odata/Observation(5)
        [Queryable]
        public SingleResult<tblObservation> GetObservation([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblObservations.Where(Observation => Observation.observationID == key));
        }

        // PUT odata/Observation(5)
        //public IHttpActionResult Put([FromODataUri] int key, Observation Observation)
        //public HttpResponseMessage Put([FromODataUri] int key, Observation Observation)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, tblObservation Observation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != Observation.Id)
            //{
            //    return BadRequest();
            //}

            db.Entry(Observation).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!ObservationExists(key))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return Updated(Observation);
            //return Request.CreateResponse<Observation[]>(HttpStatusCode.OK, new[] { Observation });
        }

        // POST odata/Observation
        //public IHttpActionResult Post(Observation Observation)
        public HttpResponseMessage Post(tblObservation Observation)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblObservations.Add(Observation);
            db.SaveChanges();

            //return Created(Observation);
            return Request.CreateResponse <tblObservation[]>(HttpStatusCode.Created, new[] { Observation });
        }

        //// PATCH odata/Observation(5)
        //[AcceptVerbs("PATCH", "MERGE")]
        //public IHttpActionResult Patch([FromODataUri] int key, Delta<Observation> patch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    Observation Observation = db.Observations.Find(key);
        //    if (Observation == null)
        //    {
        //        return NotFound();
        //    }

        //    patch.Patch(Observation);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ObservationExists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Updated(Observation);
        //}

        // DELETE odata/Observation(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblObservation Observation = db.tblObservations.Find(key);
            if (Observation == null)
            {
                return NotFound();
            }

            foreach (tblObservation rec in db.tblObservations.Where(e => e.observationID == key))
            {
                db.tblObservations.Remove(rec);
            }

            db.tblObservations.Remove(Observation);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    Observation Observation = db.Observations.Find(key);
        //    if (Observation == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Observations.Remove(Observation);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObservationExists(int key)
        {
            return db.tblObservations.Count(e => e.observationID == key) > 0;
        }
    }
}
