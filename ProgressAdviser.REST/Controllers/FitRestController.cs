﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web;
using System.Web.Mvc;
using ProgressAdviser.REST.Models;
using System.Web.Script.Serialization;
using System.Diagnostics;
using System.DirectoryServices;
using System.Web.Http.Cors;
using System.IO;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using System.Net.Http.Headers;
using ProgressAdviser.REST.Utilities;


namespace ProgressAdviser.REST.Controllers
{
    //[EnableCors(origins: "http://myclient.azurewebsites.net", headers: "*", methods: "*")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FitRestController : ApiController
    {
        private progressAdviserEntities db = new progressAdviserEntities();

        [Route("rest/GetDeficiencies/{districtID}")]
        public SerializableDataTable GetDeficiencies(string districtID)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["progressAdviserEntities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format("SELECT * FROM vFitReportData WHERE " +
                     "DISTRICTID = '{0}' AND FACILITYID IS NOT NULL AND (" +
                     "SECTION1 = 1 OR SECTION1 = 2 OR " +
                     "SECTION2 = 1 OR SECTION2 = 2 OR " + 
                     "SECTION3 = 1 OR SECTION3 = 2 OR " +
                     "SECTION4 = 1 OR SECTION4 = 2 OR " +
                     "SECTION5 = 1 OR SECTION5 = 2 OR " +
                     "SECTION6 = 1 OR SECTION6 = 2 OR " +
                     "SECTION7 = 1 OR SECTION7 = 2 OR " +
                     "SECTION8 = 1 OR SECTION8 = 2 OR " +
                     "SECTION9 = 1 OR SECTION9 = 2 OR " +
                     "SECTION10 = 1 OR SECTION10 = 2 OR " +
                     "SECTION11 = 1 OR SECTION11 = 2 OR " +
                     "SECTION12 = 1 OR SECTION12 = 2 OR " +
                     "SECTION13 = 1 OR SECTION13 = 2 OR " +
                     "SECTION14 = 1 OR SECTION14 = 2 OR " +
                     "SECTION15 = 1 OR SECTION15 = 2)" +
                      " ORDER BY reportId ASC", districtID);
                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch
            { }

            return new SerializableDataTable(dataTable);
        }

  
    }

}
