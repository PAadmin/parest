﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ProgressAdviser.REST.Models;
using System.Threading.Tasks;

namespace ProgressAdviser.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using CH2MHILL.PMP2.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SanDiegoSchool>("SanDiegoSchool");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SanDiegoSchoolController : ODataController
    {
        private progressAdviserEntities db = new progressAdviserEntities();

        // GET odata/SanDiegoSchool
        [Queryable]
        public IQueryable<tblSanDiegoSchool> GetSanDiegoSchool()
        {
            return db.tblSanDiegoSchools;
        }

        // GET odata/SanDiegoSchool(5)
        [Queryable]
        public SingleResult<tblSanDiegoSchool> GetSanDiegoSchool([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblSanDiegoSchools.Where(SanDiegoSchool => SanDiegoSchool.ID == key));
        }

        // PUT odata/SanDiegoSchool(5)
        //public IHttpActionResult Put([FromODataUri] int key, SanDiegoSchool SanDiegoSchool)
        //public HttpResponseMessage Put([FromODataUri] int key, SanDiegoSchool SanDiegoSchool)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, tblSanDiegoSchool SanDiegoSchool)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != SanDiegoSchool.Id)
            //{
            //    return BadRequest();
            //}

            db.Entry(SanDiegoSchool).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!SanDiegoSchoolExists(key))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return Updated(SanDiegoSchool);
            //return Request.CreateResponse<SanDiegoSchool[]>(HttpStatusCode.OK, new[] { SanDiegoSchool });
        }

        // POST odata/SanDiegoSchool
        //public IHttpActionResult Post(SanDiegoSchool SanDiegoSchool)
        public HttpResponseMessage Post(tblSanDiegoSchool SanDiegoSchool)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblSanDiegoSchools.Add(SanDiegoSchool);
            db.SaveChanges();

            //return Created(SanDiegoSchool);
            return Request.CreateResponse <tblSanDiegoSchool[]>(HttpStatusCode.Created, new[] { SanDiegoSchool });
        }

        //// PATCH odata/SanDiegoSchool(5)
        //[AcceptVerbs("PATCH", "MERGE")]
        //public IHttpActionResult Patch([FromODataUri] int key, Delta<SanDiegoSchool> patch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    SanDiegoSchool SanDiegoSchool = db.SanDiegoSchools.Find(key);
        //    if (SanDiegoSchool == null)
        //    {
        //        return NotFound();
        //    }

        //    patch.Patch(SanDiegoSchool);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SanDiegoSchoolExists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Updated(SanDiegoSchool);
        //}

        // DELETE odata/SanDiegoSchool(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblSanDiegoSchool SanDiegoSchool = db.tblSanDiegoSchools.Find(key);
            if (SanDiegoSchool == null)
            {
                return NotFound();
            }

            foreach (tblSanDiegoSchool rec in db.tblSanDiegoSchools.Where(e => e.ID == key))
            {
                db.tblSanDiegoSchools.Remove(rec);
            }

            db.tblSanDiegoSchools.Remove(SanDiegoSchool);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    SanDiegoSchool SanDiegoSchool = db.SanDiegoSchools.Find(key);
        //    if (SanDiegoSchool == null)
        //    {
        //        return NotFound();
        //    }

        //    db.SanDiegoSchools.Remove(SanDiegoSchool);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SanDiegoSchoolExists(int key)
        {
            return db.tblSanDiegoSchools.Count(e => e.ID == key) > 0;
        }
    }
}
