﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ProgressAdviser.REST.Models;
using System.Threading.Tasks;

namespace ProgressAdviser.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using CH2MHILL.PMP2.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<FitReportData>("FitReportData");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class FitReportDataController : ODataController
    {
        private progressAdviserEntities db = new progressAdviserEntities();

        // GET odata/FitReportData
        [Queryable]
        public IQueryable<vFitReportData> GetFitReportData()
        {
            return db.vFitReportDatas;
        }

        // GET odata/FitReportData(5)
        [Queryable]
        public SingleResult<vFitReportData> GetFitReportData([FromODataUri] int key)
        {
            return SingleResult.Create(db.vFitReportDatas.Where(FitReportData => FitReportData.reportId == key));
        }

        // PUT odata/FitReportData(5)
        //public IHttpActionResult Put([FromODataUri] int key, FitReportData FitReportData)
        //public HttpResponseMessage Put([FromODataUri] int key, FitReportData FitReportData)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, vFitReportData FitReportData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != FitReportData.Id)
            //{
            //    return BadRequest();
            //}

            db.Entry(FitReportData).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!FitReportDataExists(key))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return Updated(FitReportData);
            //return Request.CreateResponse<FitReportData[]>(HttpStatusCode.OK, new[] { FitReportData });
        }
   

        // POST odata/FitReportData
        //public IHttpActionResult Post(FitReportData FitReportData)
        public HttpResponseMessage Post(vFitReportData FitReportData)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vFitReportDatas.Add(FitReportData);
            db.SaveChanges();

            //return Created(FitReportData);
            return Request.CreateResponse <vFitReportData[]>(HttpStatusCode.Created, new[] { FitReportData });
        }

        //// PATCH odata/FitReportData(5)
        //[AcceptVerbs("PATCH", "MERGE")]
        //public IHttpActionResult Patch([FromODataUri] int key, Delta<FitReportData> patch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    FitReportData FitReportData = db.FitReportDatas.Find(key);
        //    if (FitReportData == null)
        //    {
        //        return NotFound();
        //    }

        //    patch.Patch(FitReportData);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!FitReportDataExists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Updated(FitReportData);
        //}

        // DELETE odata/FitReportData(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vFitReportData FitReportData = db.vFitReportDatas.Find(key);
            if (FitReportData == null)
            {
                return NotFound();
            }

            foreach (vFitReportData rec in db.vFitReportDatas.Where(e => e.reportId == key))
            {
                db.vFitReportDatas.Remove(rec);
            }

            db.vFitReportDatas.Remove(FitReportData);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    FitReportData FitReportData = db.FitReportDatas.Find(key);
        //    if (FitReportData == null)
        //    {
        //        return NotFound();
        //    }

        //    db.FitReportDatas.Remove(FitReportData);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FitReportDataExists(int key)
        {
            return db.vFitReportDatas.Count(e => e.reportId == key) > 0;
        }
    }
}
