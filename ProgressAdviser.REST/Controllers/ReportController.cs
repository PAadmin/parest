﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web;
using System.Web.Mvc;
using ProgressAdviser.REST.Models;
using System.Web.Script.Serialization;
using System.Diagnostics;
using System.DirectoryServices;
using System.Web.Http.Cors;
using System.IO;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using System.Net.Http.Headers;

//using iTextSharp.text.pdf;

namespace ProgressAdviser.REST.Controllers
{
    //[EnableCors(origins: "http://myclient.azurewebsites.net", headers: "*", methods: "*")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReportController : ApiController
    {
        private progressAdviserEntities db = new progressAdviserEntities();

        // Rest endpoints for creating pdfs
        [Route("rest/CreateLCAPBudgetReport/{entityId}/{yearId}/{year2Id}/{year3Id}/{reportName}")]
        public FileResult GetCreateLCAPReport(string entityId, string yearId, string year2Id, string year3Id, string reportName)
        {
            try
            {

                ReportViewer rv = new ReportViewer();
                ServerReport sr = rv.ServerReport;
                sr.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                sr.ReportPath = ConfigurationManager.AppSettings["ReportPath"] + reportName;

                string ruser = ConfigurationManager.AppSettings["ReportUser"];
                string rpassword = ConfigurationManager.AppSettings["ReportPassword"];
                string rdomain = ConfigurationManager.AppSettings["ReportDomain"];

                IReportServerCredentials irsc = new CustomReportCredentials(ruser, rpassword, rdomain);
                sr.ReportServerCredentials = irsc;

                // Report parameter 1
                ReportParameter rp1 = new ReportParameter();
                rp1.Name = "EntityId";
                rp1.Values.Add(entityId);

                // Set report parameter 1 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp1 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                // Report parameter 2
                ReportParameter rp2 = new ReportParameter();
                rp2.Name = "LcapYearId";
                rp2.Values.Add(yearId);

                // Set the report parameter 2 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp2 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                // Report parameter 3
                ReportParameter rp3 = new ReportParameter();
                rp3.Name = "LcapYearId2";
                rp3.Values.Add(year2Id);

                // Set the report parameter 3 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp3 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                // Report parameter 4
                ReportParameter rp4 = new ReportParameter();
                rp4.Name = "LcapYearId3";
                rp4.Values.Add(year3Id);

                // Set the report parameter 4 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp4 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                sr.Refresh();
                byte[] byteViewer;
                string mimeType;
                string encoding;
                string extension;
                string[] streamids;
                Warning[] warn;

                byteViewer = sr.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warn); // "Word"

                string sourceFolder = ConfigurationManager.AppSettings["ReportSourceFolder"];

                string reportFileName = reportName + "_" + entityId + "_" + yearId + "_" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ".pdf";
                reportFileName = reportFileName.Replace("\\", "_");
                reportFileName = reportFileName.Replace(@"\", "_");
                reportFileName = reportFileName.Replace("/", "-");
                reportFileName = reportFileName.Replace(" ", "_");
                reportFileName = reportFileName.Replace(":", "_");

                //string filename = Path.Combine(Path.GetTempPath(), filenameParams + ".pdf");
                //using (FileStream fs = new FileStream(filename, FileMode.Create))
                //{ fs.Write(bytes, 0, bytes.Length); }


                using (FileStream fs = new FileStream(sourceFolder + reportFileName, FileMode.Create))
                {
                    fs.Write(byteViewer, 0, byteViewer.Length);
                    fs.Close();
                }

                string reportFileWebLocation = ConfigurationManager.AppSettings["ReportUrlFolder"] + reportFileName; ;
                //"http://localhost/crcreports/" + reportFileName;

                return new FilePathResult(reportFileWebLocation, "application/pdf");
            }
            catch (Exception ex)
            {
                return new FilePathResult("Error processing: " + ex.Message + ", " + ex.InnerException + ", " + ex.StackTrace, "application/pdf");
                //return "Error processing: " + ex.Message;
            }
        }

        [Route("rest/CreateLCAPBudgetReport/{entityId}/{yearId}/{year2Id}/{year3Id}/{subgroups}/{reportName}")]
        public FileResult GetCreateLCAPReport(string entityId, string yearId, string year2Id, string year3Id, string subgroups, string reportName)
        {
            try
            {

                ReportViewer rv = new ReportViewer();
                ServerReport sr = rv.ServerReport;
                sr.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                sr.ReportPath = ConfigurationManager.AppSettings["ReportPath"] + reportName;

                string ruser = ConfigurationManager.AppSettings["ReportUser"];
                string rpassword = ConfigurationManager.AppSettings["ReportPassword"];
                string rdomain = ConfigurationManager.AppSettings["ReportDomain"];

                IReportServerCredentials irsc = new CustomReportCredentials(ruser, rpassword, rdomain);
                sr.ReportServerCredentials = irsc;

                // Report parameter 1
                ReportParameter rp1 = new ReportParameter();
                rp1.Name = "EntityId";
                rp1.Values.Add(entityId);

                // Set report parameter 1 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp1 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                // Report parameter 2
                ReportParameter rp2 = new ReportParameter();
                rp2.Name = "LcapYearId";
                rp2.Values.Add(yearId);

                // Set the report parameter 2 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp2 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                // Report parameter 3
                ReportParameter rp3 = new ReportParameter();
                rp3.Name = "LcapYearId2";
                rp3.Values.Add(year2Id);

                // Set the report parameter 3 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp3 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                // Report parameter 4
                ReportParameter rp4 = new ReportParameter();
                rp4.Name = "LcapYearId3";
                rp4.Values.Add(year3Id);

                // Set the report parameter 4 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp4 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                // Report parameter 5
                ReportParameter rp5 = new ReportParameter();
                rp5.Name = "SubgroupIdList";
                rp5.Values.Add(subgroups);

                // Set the report parameter 4 for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp5 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                sr.Refresh();
                byte[] byteViewer;
                string mimeType;
                string encoding;
                string extension;
                string[] streamids;
                Warning[] warn;

                byteViewer = sr.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warn); // "Word"

                string sourceFolder = ConfigurationManager.AppSettings["ReportSourceFolder"];

                string reportFileName = reportName + "_" + entityId + "_" + yearId + "_Subgroups_" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ".pdf";
                reportFileName = reportFileName.Replace("\\", "_");
                reportFileName = reportFileName.Replace(@"\", "_");
                reportFileName = reportFileName.Replace("/", "-");
                reportFileName = reportFileName.Replace(" ", "_");
                reportFileName = reportFileName.Replace(":", "_");

                //string filename = Path.Combine(Path.GetTempPath(), filenameParams + ".pdf");
                //using (FileStream fs = new FileStream(filename, FileMode.Create))
                //{ fs.Write(bytes, 0, bytes.Length); }


                using (FileStream fs = new FileStream(sourceFolder + reportFileName, FileMode.Create))
                {
                    fs.Write(byteViewer, 0, byteViewer.Length);
                    fs.Close();
                }

                string reportFileWebLocation = ConfigurationManager.AppSettings["ReportUrlFolder"] + reportFileName; ;
                //"http://localhost/crcreports/" + reportFileName;

                return new FilePathResult(reportFileWebLocation, "application/pdf");
            }
            catch (Exception ex)
            {
                return new FilePathResult("Error processing: " + ex.Message + ", " + ex.InnerException + ", " + ex.StackTrace, "application/pdf");
                //return "Error processing: " + ex.Message;
            }
        }

        [Route("rest/CreateLCAPReport/{entityId}/{yearId}/{reportName}")]
        public FileResult GetCreateLCAPReport(string entityId, string yearId, string reportName)
        {
            try
            {

                ReportViewer rv = new ReportViewer();
                ServerReport sr = rv.ServerReport;
                sr.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                sr.ReportPath = ConfigurationManager.AppSettings["ReportPath"] + reportName;

                string ruser = ConfigurationManager.AppSettings["ReportUser"];
                string rpassword = ConfigurationManager.AppSettings["ReportPassword"];
                string rdomain = ConfigurationManager.AppSettings["ReportDomain"];

                IReportServerCredentials irsc = new CustomReportCredentials(ruser, rpassword, rdomain);
                sr.ReportServerCredentials = irsc;

                // Report parameters
                ReportParameter rp1 = new ReportParameter();
                rp1.Name = "EntityId";
                rp1.Values.Add(entityId);

                // Set the report parameters for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp1 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
                ReportParameter rp2 = new ReportParameter();
                if (reportName.ToUpper().Contains("SPSA"))
                {
                    rp2.Name = "SpsaYearId";
                }
                else
                {
                    rp2.Name = "LcapYearId";  
                }
                rp2.Values.Add(yearId);

                // Set the report parameters for the report
                try
                {
                    sr.SetParameters(new ReportParameter[] { rp2 });
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

                sr.Refresh();
                byte[] byteViewer;
                string mimeType;
                string encoding;
                string extension;
                string[] streamids;
                Warning[] warn;

                byteViewer = sr.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warn); // "Word"

                string sourceFolder = ConfigurationManager.AppSettings["ReportSourceFolder"];

                string reportFileName = reportName + "_" + entityId + "_" + yearId + "_" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ".pdf";
                reportFileName = reportFileName.Replace("\\", "_");
                reportFileName = reportFileName.Replace(@"\", "_");
                reportFileName = reportFileName.Replace("/", "-");
                reportFileName = reportFileName.Replace(" ", "_");
                reportFileName = reportFileName.Replace(":", "_");

                //string filename = Path.Combine(Path.GetTempPath(), filenameParams + ".pdf");
                //using (FileStream fs = new FileStream(filename, FileMode.Create))
                //{ fs.Write(bytes, 0, bytes.Length); }


                using (FileStream fs = new FileStream(sourceFolder + reportFileName, FileMode.Create))
                {
                    fs.Write(byteViewer, 0, byteViewer.Length);
                    fs.Close();
                }

                string reportFileWebLocation = ConfigurationManager.AppSettings["ReportUrlFolder"] + reportFileName; ;
                //"http://localhost/crcreports/" + reportFileName;

                return new FilePathResult(reportFileWebLocation, "application/pdf");
            }
            catch (Exception ex)
            {
                return new FilePathResult("Error processing: " + ex.Message + ", " + ex.InnerException + ", " + ex.StackTrace, "application/pdf");
                //return "Error processing: " + ex.Message;
            }
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }

    public class CustomReportCredentials : IReportServerCredentials
    {
        private string _UserName;
        private string _PassWord;
        private string _DomainName;

        public CustomReportCredentials(string UserName, string PassWord, string DomainName)
        {
            _UserName = UserName;
            _PassWord = PassWord;
            _DomainName = DomainName;
        }

        public System.Security.Principal.WindowsIdentity ImpersonationUser
        {
            get { return null; }
        }

        public ICredentials NetworkCredentials
        {
            get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string user,
         out string password, out string authority)
        {
            authCookie = null;
            user = password = authority = null;
            return false;
        }
    }
}
