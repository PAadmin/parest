//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProgressAdviser.REST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSurveysResponsesDetail
    {
        public int ID { get; set; }
        public int responseID { get; set; }
        public int qID { get; set; }
        public int criteriaID { get; set; }
        public string comments { get; set; }
    
        public virtual tblSurveysRespons tblSurveysRespons { get; set; }
    }
}
