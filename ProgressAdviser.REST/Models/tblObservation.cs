//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProgressAdviser.REST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblObservation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblObservation()
        {
            this.tblObservationsGoals = new HashSet<tblObservationsGoal>();
        }
    
        public int observationID { get; set; }
        public string deviceID { get; set; }
        public Nullable<int> appObsID { get; set; }
        public Nullable<System.DateTime> startTime { get; set; }
        public Nullable<System.DateTime> timeStamp { get; set; }
        public Nullable<int> observerID { get; set; }
        public Nullable<int> districtID { get; set; }
        public Nullable<int> schoolID { get; set; }
        public Nullable<int> gradeLevelID { get; set; }
        public Nullable<int> courseID { get; set; }
        public Nullable<int> staffID { get; set; }
        public string comments { get; set; }
        public bool sendRes { get; set; }
        public Nullable<bool> isFormal { get; set; }
        public Nullable<System.DateTime> lastUpdated { get; set; }
        public Nullable<int> updatedByUser { get; set; }
        public Nullable<bool> isFinal { get; set; }
        public Nullable<bool> isSurvey { get; set; }
        public Nullable<int> surveyID { get; set; }
        public int isLCAP { get; set; }
        public int isSPSA { get; set; }
        public string ipAddress { get; set; }
        public Nullable<bool> isFIT { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblObservationsGoal> tblObservationsGoals { get; set; }
    }
}
