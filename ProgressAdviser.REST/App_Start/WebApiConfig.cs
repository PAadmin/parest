﻿
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

using System.Web.Http.OData.Builder;
using ProgressAdviser.REST.Models;


namespace ProgressAdviser.REST
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Step 1
            // http://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2
            // Step 2
            // http://stackoverflow.com/questions/19575600/why-do-i-get-an-invalidoperationexception-when-i-try-to-use-attribute-routing-wi

            // Default  to JSON Reponse
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            // Default webapi routes
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
           
            // Use Attribute Routes
            config.MapHttpAttributeRoutes();

            // Map odata endpoints
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<tblObservation>("Observation");
            builder.EntitySet<tblObservationsGoal>("ObservationsGoal");

            //builder.EntitySet<tblSanDiegoSchool>("SanDiegoSchool");
            builder.EntitySet<vFitReportData>("FitReportData");
            builder.EntitySet<tblFitReportData>("FitReportDataTable");

            config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());

            // Enable CORs
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
             
        }
    }
}
